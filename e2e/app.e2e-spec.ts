import { Ng2PartsPage } from './app.po';

describe('ng2-parts App', function() {
  let page: Ng2PartsPage;

  beforeEach(() => {
    page = new Ng2PartsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
