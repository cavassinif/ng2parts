import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng2-module',
  templateUrl: './ng2-module.component.html',
  styleUrls: ['./ng2-module.component.css']
})
export class Ng2ModuleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
