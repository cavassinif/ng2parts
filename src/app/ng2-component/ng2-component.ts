import { AngularFire, FirebaseListObservable } from 'angularfire2';

export class Ng2Component {
  components: FirebaseListObservable<any>;
  $key: string;
  name: string;
  color: string;
  x: number;
  y: number;
  width: number;
  height: number;
  constructor(af: AngularFire) {
    this.components = af.database.list('/components');
  }
  public save(id: string) {
    this.components.update(id, this);
  }
}
