import { Component, Input, OnInit, ElementRef, Renderer, OnDestroy, OnChanges, SimpleChange } from '@angular/core';
import { Ng2Component } from './ng2-component';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-ng2-component',
  host: {
    '(dragstart)': 'onDragStart($event)',
    '(dragend)': 'onDragEnd($event)',
    '(drag)': 'onDrag($event)'
  },
  templateUrl: './ng2-component.component.html',
  styleUrls: ['./ng2-component.component.css']
})
export class Ng2ComponentComponent implements OnDestroy, OnInit, OnChanges {
  components: FirebaseListObservable<any>;
  @Input() ng2Component: Ng2Component;

  constructor(private el: ElementRef, private renderer: Renderer, af: AngularFire) {
    this.components = af.database.list('/components');
  }

  public ngOnInit(): void {
    this.renderer.setElementAttribute(this.el.nativeElement, 'draggable', 'true');
  }
  onDragStart(event: MouseEvent) {
    this.ng2Component.x = event.x - this.el.nativeElement.offsetLeft;
    this.ng2Component.y = event.y - this.el.nativeElement.offsetTop;
  }
  onDrag(event: MouseEvent) {
    this.doTranslation(event.x, event.y);
  }
  onDragEnd(event: MouseEvent) {

    this.ng2Component.x = event.x - this.el.nativeElement.offsetLeft;
    this.ng2Component.y = event.y - this.el.nativeElement.offsetTop;
    // this.ng2Component.save(this.ng2Component.$key)

    this.components.update(this.ng2Component.$key, { x: this.ng2Component.x, y: this.ng2Component.y });

  }
  doTranslation(x: number, y: number) {
    if (!x || !y) return;
    this.renderer.setElementStyle(this.el.nativeElement, 'top', (y - this.ng2Component.y) + 'px');
    this.renderer.setElementStyle(this.el.nativeElement, 'left', (x - this.ng2Component.x) + 'px');
  }
  public ngOnDestroy(): void {
    this.renderer.setElementAttribute(this.el.nativeElement, 'draggable', 'false');
  }
  getPx(value) {
    return value + 'px';
  }
  ngOnChanges(changes: { [propName: string]: SimpleChange }) {
    console.log('ngOnChanges - myProp = ' +
      changes['ng2Component'].currentValue);
  }
}
