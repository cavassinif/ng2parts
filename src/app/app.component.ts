import { Component, OnInit } from '@angular/core';
import { Ng2Component } from './ng2-component/ng2-component';
import { Ng2Module } from './ng2-module/ng2-module';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  components: FirebaseListObservable<any[]>;
  constructor(af: AngularFire) {
    this.components = af.database.list('/components');
  }
  // components: Ng2Component[];
  ngOnInit() {
    // this.components = [];
    // this.components.push({ name: 'component1', x: 10, y: 10, color: 'red', width: 150, height: 300 })
    // this.components.push({ name: 'component2', x: 20, y: 100, color: 'blue', width: 150, height: 150 })
    // this.components.push({ name: 'component3', x: 30, y: 200, color: 'yellow', width: 150, height: 100 })
    // this.components.push({ name: 'component4', x: 40, y: 300, color: 'gray', width: 150, height: 200 })
    // this.components.push({ name: 'component5', x: 10, y: 10, color: 'orange', width: 150, height: 240 })
    // this.components.push({ name: 'component6', x: 10, y: 10, color: 'green', width: 150, height: 120 })
  }
}
