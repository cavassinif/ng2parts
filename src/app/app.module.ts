import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { Ng2ComponentComponent } from './ng2-component/ng2-component.component';
import { Ng2ModuleComponent } from './ng2-module/ng2-module.component';
import { Ng2Component } from './ng2-component/ng2-component';
import { Ng2Module } from './ng2-module/ng2-module';
import { AngularFireModule } from 'angularfire2';

// Must export the config
export const firebaseConfig = {
  apiKey: "AIzaSyDOP9qgMIKwI_Ru8rFNrFhqjSepsnIzQSc",
  authDomain: "ng2parts.firebaseapp.com",
  databaseURL: "https://ng2parts.firebaseio.com",
  storageBucket: "ng2parts.appspot.com",
  messagingSenderId: "361807125610"
};

@NgModule({
  declarations: [
    AppComponent,
    Ng2ComponentComponent,
    Ng2ModuleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
